import { Navbar } from 'react-bootstrap';
import React, { Component, Fragment } from 'react';
import { bindActionCreators, compose } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import NewsList from './containers/NewsList';
import { getNews } from './actions/index';
import './App.css';

class EntryComponent extends Component {
	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(getNews());
	}

	generate(array = [], element) {
		console.log(array);
		return array.map(value =>
			React.cloneElement(element, {
				key: value
			}));
	}

	render() {
		const { news } = this.props;
		return (
      <Fragment>
		<Navbar>
			<Navbar.Header>
				<Navbar.Brand>
				<a className="header" href="#home">React News App</a>
				</Navbar.Brand>
			</Navbar.Header>
		</Navbar>
        <div>
          <div className="border-list">
            <NewsList news={news} />
          </div>
        </div>
      </Fragment>
		);
	}
}

function mapStateToProps(state) {
	console.log(state);
	return {
		news: state.news.data.news
	};
}

function mapDispatchToProps(dispatch) {
	const actions = bindActionCreators({ getNews });
	return { ...actions, dispatch };
}

EntryComponent.propTypes = {
	dispatch: PropTypes.func.isRequired,
	news: PropTypes.array
};
const composedContainer = compose(
		connect(
			mapStateToProps,
			mapDispatchToProps
		)
	)(EntryComponent);
export { composedContainer as EntryComponent };
