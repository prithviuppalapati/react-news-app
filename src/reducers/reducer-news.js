/*
 * The users reducer will always return an array of users no matter what
 * You need to return something, so if there are no users then just return an empty array
 * */

const initialState = {
	isLoading: false,
	data: {
		news: []
	}
};

export default function (previousState = initialState, action) {
	switch (action.type) {
		case 'GET_NEWS_SUCCEED':
			return {
				...previousState,
				data: {
					...previousState.data,
					news: action.payload.articles
				}
			};
		default:
			return previousState;
	}
}
