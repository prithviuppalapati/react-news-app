import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import * as rootReducer from './reducers/index';


const combinerRootReducer = combineReducers({
	...rootReducer,
	router: routerReducer
});

const historyOptions = {};

const history = createHistory(historyOptions);

function configureStore() {
	const middleware = [thunk];

	let composeEnhancers;
	if (process.env.NODE_ENV !== 'production') {
		/* eslint-disable global-require */
		const { logger } = require('redux-logger');
		middleware.push(logger);

		const { composeWithDevTools } = require('redux-devtools-extension');
		composeEnhancers = composeWithDevTools({
			// Specify here name, actionsBlacklist, actionsCreators and other options if needed
		});
	}

	const finalEnhancer = composeEnhancers || compose;
	const store = createStore(combinerRootReducer, finalEnhancer(applyMiddleware(...middleware)));

	return store;
}

// Singleton Store instance
let mainStore;

function getStoreInstance() {
	if (typeof mainStore === 'undefined') {
		mainStore = configureStore();
	}
	return mainStore;
}

export { history, getStoreInstance };
