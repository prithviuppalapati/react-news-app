import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { getStoreInstance } from './store';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';


import './index.scss';
import './_shared.scss';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const store = getStoreInstance();
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();
