import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import './index.css';

class NewsList extends Component {
	render() {
		const { news } = this.props;
		return (
			<div className="news-container">
				{news && news.map(item => (
					<Fragment>
						<div className="news-item" key={item.title}>
							<img className="tile-img" src={item.urlToImage} alt="No Images Available" />
							<div className="title">
								<a href={item.url} target="_blank">
									{item.title}
								</a>
							</div>

							<div className="description">
								{item.description}
							</div>
						</div>
					</Fragment>
				))}
			</div>
		);
	}
}

NewsList.propTypes = {
	news: PropTypes.array.isRequired
};

export default NewsList;