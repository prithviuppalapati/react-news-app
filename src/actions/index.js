import ApiUtils from './../utils/api';

export function getNews() {
	return function (dispatch) {
		return ApiUtils.makeAjaxRequest(
			{
				url: 'https://newsapi.org/v2/top-headlines?country=in&apiKey=109cae4eebb6452ab277f6a6b05cf9cf',
				method: ApiUtils.METHOD_GET
			},
			res => {
				if (res.status === ApiUtils.RESPONSE_SUCCESS) {
					dispatch({
						type: 'GET_NEWS_SUCCEED',
						payload: res
					});
				} else {
					dispatch({
						type: 'GET_NEWS_FAILURE',
						payload: res.message
					});
				}
			},
			err => {
				dispatch({
					type: 'GET_NEWS_FAILURE',
					payload: err
				});
			}
		);
	};
}
