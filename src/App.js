/* App React Component
** Renders the Main Application
*/

// Packages
import React, { Component } from 'react';

// Components
import { EntryComponent } from './entry';

// Styles
import './index.css';

class App extends Component {
	render() {
		return (
      <EntryComponent />
		);
	}
}

export default App;
